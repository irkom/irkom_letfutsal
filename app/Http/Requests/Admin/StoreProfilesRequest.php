<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfilesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_profile_id' => 'required',
            'fullname' => 'max:50|required',
            'birthdate' => 'required|date_format:'.config('app.date_format').'|unique:profiles,birthdate',
            'handphone_no' => 'max:20|required',
        ];
    }
}

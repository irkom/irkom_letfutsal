<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreTeamsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:10|required|unique:teams,name,'.$this->route('team'),
            'team_avatar' => 'required',
            'player_team' => 'required',
            'player_team.*' =>'exists:profiles,id|unique:profile_team,profile_id',
        ];
    }
}

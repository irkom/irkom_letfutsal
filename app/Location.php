<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Location
 *
 * @package App
 * @property string $name
*/
class Location extends Model
{
    use SoftDeletes;

    protected $fillable = ['name'];
    
    
}

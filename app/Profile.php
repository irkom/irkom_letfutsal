<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Profile
 *
 * @package App
 * @property string $user_profile
 * @property string $fullname
 * @property string $birthdate
 * @property string $avatar
 * @property string $handphone_no
*/
class Profile extends Model
{
    use SoftDeletes;

    protected $fillable = ['fullname', 'birthdate', 'avatar', 'handphone_no', 'user_profile_id'];
    

    /**
     * Set to null if empty
     * @param $input
     */
    public function setUserProfileIdAttribute($input)
    {
        $this->attributes['user_profile_id'] = $input ? $input : null;
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setBirthdateAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['birthdate'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['birthdate'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getBirthdateAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }
    
    public function user_profile()
    {
        return $this->belongsTo(User::class, 'user_profile_id');
    }
    
}

<?php

namespace App\Providers;

use App\Role;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = \Auth::user();

        
        // Auth gates for: User management
        Gate::define('user_management_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Roles
        Gate::define('role_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Users
        Gate::define('user_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Category
        Gate::define('category_access', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('category_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('category_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('category_view', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('category_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Profile
        Gate::define('profile_access', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('profile_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('profile_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('profile_view', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('profile_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Location
        Gate::define('location_access', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('location_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('location_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('location_view', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('location_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Team
        Gate::define('team_access', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('team_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('team_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('team_view', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('team_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Station
        Gate::define('station_access', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('station_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('station_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('station_view', function ($user) {
            return in_array($user->role_id, [1, 3]);
        });
        Gate::define('station_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

    }
}

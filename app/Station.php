<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Station
 *
 * @package App
 * @property string $name
 * @property string $location
 * @property text $address
 * @property string $contact_no
 * @property enum $status
*/
class Station extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'address', 'contact_no', 'status', 'location_id'];
    

    public static $enum_status = ["Open" => "Open", "Close" => "Close", "Not Available" => "Not Available"];

    /**
     * Set to null if empty
     * @param $input
     */
    public function setLocationIdAttribute($input)
    {
        $this->attributes['location_id'] = $input ? $input : null;
    }
    
    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id')->withTrashed();
    }
    
}

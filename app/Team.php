<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 *
 * @package App
 * @property string $name
 * @property string $team_avatar
*/
class Team extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'team_avatar'];
    
    
    public function player_team()
    {
        return $this->belongsToMany(Profile::class, 'profile_team')->withTrashed();
    }
    
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1500361077ProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('profiles')) {
            Schema::create('profiles', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_profile_id')->unsigned()->nullable();
                $table->foreign('user_profile_id', '54196_596db175a6835')->references('id')->on('users')->onDelete('cascade');
                $table->string('fullname');
                $table->date('birthdate')->nullable();
                $table->string('avatar')->nullable();
                $table->string('handphone_no');
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}

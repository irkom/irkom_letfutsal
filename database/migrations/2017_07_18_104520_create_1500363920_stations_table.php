<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1500363920StationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('stations')) {
            Schema::create('stations', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('location_id')->unsigned()->nullable();
                $table->foreign('location_id', '54208_596dbc905219b')->references('id')->on('locations')->onDelete('cascade');
                $table->text('address');
                $table->string('contact_no')->nullable();
                $table->enum('status', ["Open","Close","Not Available"]);
                
                $table->timestamps();
                $table->softDeletes();

                $table->index(['deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}

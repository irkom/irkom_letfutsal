<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create596dbaa7075b5ProfileTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('profile_team')) {
            Schema::create('profile_team', function (Blueprint $table) {
                $table->integer('profile_id')->unsigned()->nullable();
                $table->foreign('profile_id', 'fk_p_54196_54204_team_pro_596dbaa7076be')->references('id')->on('profiles')->onDelete('cascade');
                $table->integer('team_id')->unsigned()->nullable();
                $table->foreign('team_id', 'fk_p_54204_54196_profile__596dbaa707748')->references('id')->on('teams')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_team');
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(LocationSeed::class);
        $this->call(RoleSeed::class);
        $this->call(UserSeed::class);
        $this->call(ProfileSeed::class);
        $this->call(StationSeed::class);
        $this->call(TeamSeed::class);
        $this->call(TeamSeedPivot::class);

    }
}

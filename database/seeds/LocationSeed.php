<?php

use Illuminate\Database\Seeder;

class LocationSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'name' => 'Jakarta',],

        ];

        foreach ($items as $item) {
            \App\Location::create($item);
        }
    }
}

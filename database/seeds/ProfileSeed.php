<?php

use Illuminate\Database\Seeder;

class ProfileSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'user_profile_id' => 1, 'fullname' => 'admin', 'birthdate' => '18-07-2017', 'avatar' => '/tmp/phpgmpJGG', 'handphone_no' => '0856323132',],

        ];

        foreach ($items as $item) {
            \App\Profile::create($item);
        }
    }
}

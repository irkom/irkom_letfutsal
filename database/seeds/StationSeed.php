<?php

use Illuminate\Database\Seeder;

class StationSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'name' => 'Kuningan', 'location_id' => 1, 'address' => 'test', 'contact_no' => '123', 'status' => 'Open',],

        ];

        foreach ($items as $item) {
            \App\Station::create($item);
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class TeamSeedPivot extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            1 => [
                'player_team' => [1],
            ],

        ];

        foreach ($items as $id => $item) {
            $team = \App\Team::find($id);

            foreach ($item as $key => $ids) {
                $team->{$key}()->sync($ids);
            }
        }
    }
}

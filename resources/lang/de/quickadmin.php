<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',		],	],
		'category' => [		'title' => 'Category',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'profile' => [		'title' => 'Profile',		'created_at' => 'Time',		'fields' => [			'user-profile' => 'User Profile',			'fullname' => 'Full Name',			'birthdate' => 'Birth Date',			'avatar' => 'Avatar',			'handphone-no' => 'Handphone No',		],	],
		'location' => [		'title' => 'Location',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'team' => [		'title' => 'Team',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'team-avatar' => 'Team avatar',			'player-team' => 'Player team',		],	],
		'station' => [		'title' => 'Station',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'location' => 'Location',			'address' => 'Address',			'contact-no' => 'Contact no',			'status' => 'Status',		],	],
	'qa_create' => 'Erstellen',
	'qa_save' => 'Speichern',
	'qa_edit' => 'Bearbeiten',
	'qa_view' => 'Betrachten',
	'qa_update' => 'Aktualisieren',
	'qa_list' => 'Listen',
	'qa_no_entries_in_table' => 'Keine Einträge in Tabelle',
	'custom_controller_index' => 'Custom controller index.',
	'qa_logout' => 'Abmelden',
	'qa_add_new' => 'Hinzufügen',
	'qa_are_you_sure' => 'Sind Sie sicher?',
	'qa_back_to_list' => 'Zurück zur Liste',
	'qa_dashboard' => 'Dashboard',
	'qa_delete' => 'Löschen',
	'quickadmin_title' => 'letsfutsal',
];
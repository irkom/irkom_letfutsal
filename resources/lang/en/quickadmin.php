<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',		],	],
		'category' => [		'title' => 'Category',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'profile' => [		'title' => 'Profile',		'created_at' => 'Time',		'fields' => [			'user-profile' => 'User Profile',			'fullname' => 'Full Name',			'birthdate' => 'Birth Date',			'avatar' => 'Avatar',			'handphone-no' => 'Handphone No',		],	],
		'location' => [		'title' => 'Location',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'team' => [		'title' => 'Team',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'team-avatar' => 'Team avatar',			'player-team' => 'Player team',		],	],
		'station' => [		'title' => 'Station',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'location' => 'Location',			'address' => 'Address',			'contact-no' => 'Contact no',			'status' => 'Status',		],	],
	'qa_create' => 'Create',
	'qa_save' => 'Save',
	'qa_edit' => 'Edit',
	'qa_restore' => 'Restore',
	'qa_permadel' => 'Delete Permanently',
	'qa_view' => 'View',
	'qa_update' => 'Update',
	'qa_list' => 'List',
	'qa_no_entries_in_table' => 'No entries in table',
	'custom_controller_index' => 'Custom controller index.',
	'qa_logout' => 'Logout',
	'qa_add_new' => 'Add new',
	'qa_are_you_sure' => 'Are you sure?',
	'qa_back_to_list' => 'Back to list',
	'qa_dashboard' => 'Dashboard',
	'qa_delete' => 'Delete',
	'quickadmin_title' => 'letsfutsal',
];
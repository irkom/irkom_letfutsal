<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',		],	],
		'category' => [		'title' => 'Category',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'profile' => [		'title' => 'Profile',		'created_at' => 'Time',		'fields' => [			'user-profile' => 'User Profile',			'fullname' => 'Full Name',			'birthdate' => 'Birth Date',			'avatar' => 'Avatar',			'handphone-no' => 'Handphone No',		],	],
		'location' => [		'title' => 'Location',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'team' => [		'title' => 'Team',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'team-avatar' => 'Team avatar',			'player-team' => 'Player team',		],	],
		'station' => [		'title' => 'Station',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'location' => 'Location',			'address' => 'Address',			'contact-no' => 'Contact no',			'status' => 'Status',		],	],
	'qa_create' => 'Crear',
	'qa_save' => 'Guardar',
	'qa_edit' => 'Editar',
	'qa_view' => 'Ver',
	'qa_update' => 'Actualizar',
	'qa_list' => 'Listar',
	'qa_no_entries_in_table' => 'Sin valores en la tabla',
	'custom_controller_index' => 'Índice del controlador personalizado (index).',
	'qa_logout' => 'Salir',
	'qa_add_new' => 'Agregar',
	'qa_are_you_sure' => 'Estás seguro?',
	'qa_back_to_list' => 'Regresar a la lista?',
	'qa_dashboard' => 'Tablero',
	'qa_delete' => 'Eliminar',
	'quickadmin_title' => 'letsfutsal',
];
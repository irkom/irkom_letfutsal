<?php

return [
		'user-management' => [		'title' => 'User Management',		'created_at' => 'Time',		'fields' => [		],	],
		'roles' => [		'title' => 'Roles',		'created_at' => 'Time',		'fields' => [			'title' => 'Title',		],	],
		'users' => [		'title' => 'Users',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'email' => 'Email',			'password' => 'Password',			'role' => 'Role',			'remember-token' => 'Remember token',		],	],
		'category' => [		'title' => 'Category',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'profile' => [		'title' => 'Profile',		'created_at' => 'Time',		'fields' => [			'user-profile' => 'User Profile',			'fullname' => 'Full Name',			'birthdate' => 'Birth Date',			'avatar' => 'Avatar',			'handphone-no' => 'Handphone No',		],	],
		'location' => [		'title' => 'Location',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',		],	],
		'team' => [		'title' => 'Team',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'team-avatar' => 'Team avatar',			'player-team' => 'Player team',		],	],
		'station' => [		'title' => 'Station',		'created_at' => 'Time',		'fields' => [			'name' => 'Name',			'location' => 'Location',			'address' => 'Address',			'contact-no' => 'Contact no',			'status' => 'Status',		],	],
	'qa_create' => 'Criar',
	'qa_save' => 'Salvar',
	'qa_edit' => 'Editar',
	'qa_view' => 'Visualizar',
	'qa_update' => 'Atualizar',
	'qa_list' => 'Listar',
	'qa_no_entries_in_table' => 'Sem entradas na tabela',
	'custom_controller_index' => 'Índice de Controller personalizado.',
	'qa_logout' => 'Sair',
	'qa_add_new' => 'Novo',
	'qa_are_you_sure' => 'Tem certeza?',
	'qa_back_to_list' => 'Voltar',
	'qa_dashboard' => 'Painel',
	'qa_delete' => 'Excluir',
	'quickadmin_title' => 'letsfutsal',
];
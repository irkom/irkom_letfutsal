@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.profile.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.profiles.store'], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('user_profile_id', 'User Profile*', ['class' => 'control-label']) !!}
                    {!! Form::select('user_profile_id', $user_profiles, old('user_profile_id'), ['class' => 'form-control select2', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('user_profile_id'))
                        <p class="help-block">
                            {{ $errors->first('user_profile_id') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('fullname', 'Full Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('fullname', old('fullname'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('fullname'))
                        <p class="help-block">
                            {{ $errors->first('fullname') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('birthdate', 'Birth Date*', ['class' => 'control-label']) !!}
                    {!! Form::text('birthdate', old('birthdate'), ['class' => 'form-control date', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('birthdate'))
                        <p class="help-block">
                            {{ $errors->first('birthdate') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('avatar', 'Avatar', ['class' => 'control-label']) !!}
                    {!! Form::file('avatar', ['class' => 'form-control', 'style' => 'margin-top: 4px;']) !!}
                    {!! Form::hidden('avatar_max_size', 8) !!}
                    {!! Form::hidden('avatar_max_width', 4000) !!}
                    {!! Form::hidden('avatar_max_height', 4000) !!}
                    <p class="help-block"></p>
                    @if($errors->has('avatar'))
                        <p class="help-block">
                            {{ $errors->first('avatar') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('handphone_no', 'Handphone No*', ['class' => 'control-label']) !!}
                    {!! Form::text('handphone_no', old('handphone_no'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('handphone_no'))
                        <p class="help-block">
                            {{ $errors->first('handphone_no') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

@section('javascript')
    @parent
    <script>
        $('.date').datepicker({
            autoclose: true,
            dateFormat: "{{ config('app.date_format_js') }}"
        });
    </script>

@stop
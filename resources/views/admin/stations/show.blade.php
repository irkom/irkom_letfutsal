@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.station.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.station.fields.name')</th>
                            <td>{{ $station->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.station.fields.location')</th>
                            <td>{{ $station->location->name or '' }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.station.fields.address')</th>
                            <td>{!! $station->address !!}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.station.fields.contact-no')</th>
                            <td>{{ $station->contact_no }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.station.fields.status')</th>
                            <td>{{ $station->status }}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.stations.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
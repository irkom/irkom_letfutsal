@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.team.title')</h3>
    
    {!! Form::model($team, ['method' => 'PUT', 'route' => ['admin.teams.update', $team->id], 'files' => true,]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    @if ($team->team_avatar)
                        <a href="{{ asset('uploads/'.$team->team_avatar) }}" target="_blank"><img src="{{ asset('uploads/thumb/'.$team->team_avatar) }}"></a>
                    @endif
                    {!! Form::label('team_avatar', 'Team avatar', ['class' => 'control-label']) !!}
                    {!! Form::file('team_avatar', ['class' => 'form-control', 'style' => 'margin-top: 4px;', 'required' => '']) !!}
                    {!! Form::hidden('team_avatar_max_size', 8) !!}
                    {!! Form::hidden('team_avatar_max_width', 4000) !!}
                    {!! Form::hidden('team_avatar_max_height', 4000) !!}
                    <p class="help-block"></p>
                    @if($errors->has('team_avatar'))
                        <p class="help-block">
                            {{ $errors->first('team_avatar') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('player_team', 'Player team*', ['class' => 'control-label']) !!}
                    {!! Form::select('player_team[]', $player_teams, old('player_team') ? old('player_team') : $team->player_team->pluck('id')->toArray(), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('player_team'))
                        <p class="help-block">
                            {{ $errors->first('player_team') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop


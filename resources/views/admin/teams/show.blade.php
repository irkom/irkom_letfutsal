@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.team.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.team.fields.name')</th>
                            <td>{{ $team->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.team.fields.team-avatar')</th>
                            <td>@if($team->team_avatar)<a href="{{ asset('uploads/' . $team->team_avatar) }}" target="_blank"><img src="{{ asset('uploads/thumb/' . $team->team_avatar) }}"/></a>@endif</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.team.fields.player-team')</th>
                            <td>
                                @foreach ($team->player_team as $singlePlayerTeam)
                                    <span class="label label-info label-many">{{ $singlePlayerTeam->fullname }}</span>
                                @endforeach
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('admin.teams.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
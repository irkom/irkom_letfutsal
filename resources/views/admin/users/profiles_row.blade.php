<tr data-index="{{ $index }}">
    <td>{!! Form::text('profiles['.$index.'][fullname]', old('profiles['.$index.'][fullname]', isset($field) ? $field->fullname: ''), ['class' => 'form-control']) !!}</td>
<td>{!! Form::text('profiles['.$index.'][handphone_no]', old('profiles['.$index.'][handphone_no]', isset($field) ? $field->handphone_no: ''), ['class' => 'form-control']) !!}</td>

    <td>
        <a href="#" class="remove btn btn-xs btn-danger">@lang('quickadmin.qa_delete')</a>
    </td>
</tr>